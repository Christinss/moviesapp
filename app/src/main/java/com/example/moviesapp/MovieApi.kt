package com.example.moviesapp

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


//client class that is receiving data objects from a service objects class
interface MovieApi {

    //GET list of movies in a category

    @GET("movie/popular")
    fun getMovieListPopular(
        @Query("page") page: Int
    ): Call<MovieResponse>

    //GET list of movies in a category
    @GET("movie/top_rated")
    fun getMovieListTopRated(
        @Query("page") page: Int
    ): Call<MovieResponse>

    //GET movie genres
    @GET("movie/{movie_id}")
    fun getMovieId(
        @Path("movie_id") movieId: Int
    ): Call<MovieDetails>
}