package com.example.moviesapp

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


@Parcelize
data class Movie(
    val id: Int?,
    val title: String?,
    val poster_path: String?
) : Parcelable
