package com.example.moviesapp

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


// Here ":" symbol indicates that PopularFragment is a child class of Fragment class
class PopularFragment : Fragment() {

    private val movies: MutableList<Movie> = mutableListOf()
    private var adapter = MoviesRecyclerViewAdapter()
    private var isLoading = false
    private var hasMore = true
    private var page = 1


    private val movieRecyclerViewPagingHelper: MovieRecyclerViewPagingHelper =
        MovieRecyclerViewPagingHelper()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_popular, container, false)
        val recyclerview = rootView.findViewById(R.id.recycler_view) as RecyclerView


        // Span count by orientation
        val orientation = resources.configuration.orientation
        val spanCount: Int = if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            // Code for portrait mode
            2
        } else {
            // Code for landscape mode
            3
        }

        recyclerview.layoutManager = GridLayoutManager(activity, spanCount)
        recyclerview.adapter = adapter
        adapter.onItemClickListener = object : OnItemClickListener {
            override fun onItemClick(position: Int) {
                val movies = movies[position]


                val intent = Intent(context, DetailsActivity::class.java).apply {
                    putExtra(DetailsActivity.EXTRA_KEY_MOVIE_ID, movies.id)
                }

                context?.startActivity(intent)
            }

        }
        movieRecyclerViewPagingHelper.attach(recyclerview) {
            if (isLoading || !hasMore) {
                return@attach
            }

            page++

            getMovieData(page) { newMovies ->
                hasMore = newMovies.isNotEmpty()
                movies.addAll(newMovies)
                adapter.movies = movies
            }
        }


        getMovieData(page) { newMovies ->
            movies.clear()
            movies.addAll(newMovies)

            adapter.movies = movies
        }
        return rootView
    }

    private fun getMovieData(page: Int, callback: (List<Movie>) -> Unit) {
        isLoading = true
        val apiService = MovieApiService.getInstance().create(MovieApi::class.java)
        apiService.getMovieListPopular(page).enqueue(object : Callback<MovieResponse> {
            override fun onResponse(call: Call<MovieResponse>, response: Response<MovieResponse>) {
                isLoading = false
                response.body()?.let {
                    callback.invoke(it.movies)
                }
            }

            override fun onFailure(call: Call<MovieResponse>, t: Throwable) {
                isLoading = false
            }

        })
    }
}