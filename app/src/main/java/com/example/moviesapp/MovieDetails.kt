package com.example.moviesapp

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class MovieDetails(
    val id: Int?,
    val title: String?,
    val poster_path: String?,
    val backdrop_path: String?,
    val release_date: String?,
    val genres: List<Genre>,
    val overview: String?
) : Parcelable

