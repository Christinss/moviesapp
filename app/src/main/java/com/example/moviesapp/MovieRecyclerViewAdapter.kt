package com.example.moviesapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.moviesapp.databinding.MovieItemBinding


interface OnItemClickListener {

    fun onItemClick(position: Int)
}

class MoviesRecyclerViewAdapter :
    RecyclerView.Adapter<MoviesRecyclerViewAdapter.MovieViewHolder>() {

    var movies: List<Movie>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var onItemClickListener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val itemView: View = LayoutInflater.from(parent.context).inflate(R.layout.movie_item, parent, false)
        return MovieViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        movies?.get(position)?.let { movie ->
            holder.bindMovie(movie)
        }
    }

    override fun getItemCount(): Int {
        return movies?.size ?: 0
    }

    inner class MovieViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val binding = MovieItemBinding.bind(itemView)
        private val imageBase = "https://image.tmdb.org/t/p/w500/"

        init {

            itemView.setOnClickListener {
                onItemClickListener?.onItemClick(bindingAdapterPosition)
            }
        }


        fun bindMovie(movies: Movie) {
            with(binding) {
                tvTitle.text = movies.title
                Glide.with(itemView).load(imageBase + movies.poster_path).into(movieImg)
            }
        }

    }
}

