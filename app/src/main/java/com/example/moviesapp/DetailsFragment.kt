package com.example.moviesapp

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class DetailsFragment : Fragment() {


    //Widgets
    private lateinit var posterImage: ImageView
    private lateinit var backdropImage: ImageView
    private lateinit var titleDetails: TextView
    private lateinit var descDetails: TextView
    private lateinit var releaseDate: TextView
    private lateinit var genres: TextView
    private lateinit var toolbarDetails: Toolbar

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_details, container, false)


        posterImage = view.findViewById(R.id.iv_poster)
        backdropImage = view.findViewById(R.id.iv_backdrop)
        titleDetails = view.findViewById(R.id.tv_titleDetails)
        descDetails = view.findViewById(R.id.tv_description)
        releaseDate = view.findViewById(R.id.tv_releaseDate)
        genres = view.findViewById(R.id.tv_movieGenre)


        val movieId = arguments?.getInt(DetailsActivity.EXTRA_KEY_MOVIE_ID)

        movieId?.let {

            getMovieDetails(movieId) {
                run { genres.text = it.genres.joinToString { "${it.name}" }
                    titleDetails.text = it.title
                    descDetails.text = it.overview
                    releaseDate.text = it.release_date
                    Glide.with(this)
                        .load("https://image.tmdb.org/t/p/w500/"
                                + it.poster_path)
                        .into(posterImage)

                    Glide.with(this)
                        .load("https://image.tmdb.org/t/p/w500/"
                                + it.backdrop_path)
                        .into(backdropImage)
                }
            }
        }
        println(movieId)


        toolbarDetails = view.findViewById(R.id.toolbar_details)
        val localActivity = activity
        if (localActivity is DetailsActivity) {
            localActivity.setSupportActionBar(toolbarDetails)
            localActivity.supportActionBar?.setDisplayShowHomeEnabled(true)
            localActivity.supportActionBar?.setDisplayHomeAsUpEnabled(true)
            localActivity.supportActionBar?.title = "Movie details"

        }
        return view
    }

    private fun getMovieDetails(movieId: Int, callback:(MovieDetails) -> Unit) {
        val apiService = MovieApiService.getInstance().create(MovieApi::class.java)
        apiService.getMovieId(movieId).enqueue(object : Callback<MovieDetails> {
            override fun onResponse(call: Call<MovieDetails>, response: Response<MovieDetails>) {
                response.body()?.let {
                    callback.invoke(it)
                }
                Log.d("Tag", "onResponse: ${response.body()}")
            }
            override fun onFailure(call: Call<MovieDetails>, t: Throwable) {

            }

        })
    }
}


