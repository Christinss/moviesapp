package com.example.moviesapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class DetailsActivity : AppCompatActivity() {


    companion object {

        const val EXTRA_KEY_MOVIE = "com.example.moviesapp.DetailsActivity.EXTRA_KEY_MOVIE"
        const val EXTRA_KEY_MOVIE_ID ="com.example.moviesapp.DetailsActivity.EXTRA_KEY_MOVIE_ID"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)


        val detailsFragment = DetailsFragment().also {
            it.arguments = intent.extras
        }
        detailsFragment.setHasOptionsMenu(true)

        if(savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.root_container, detailsFragment)
                .commitAllowingStateLoss()
        }
    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }
}
