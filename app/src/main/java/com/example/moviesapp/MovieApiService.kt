package com.example.moviesapp

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class MovieApiService {

    companion object {

        private const val BASE_URL = "https://api.themoviedb.org/3/"
        private const val API_KEY = "fe3b8cf16d78a0e23f0c509d8c37caad"

        private var retrofit: Retrofit? = null


        private fun getOkHttpClient(): OkHttpClient {

            val httpClient = OkHttpClient.Builder()

            httpClient.addInterceptor { chain ->
                val originalRequest = chain.request()

                val originalHttpUrl = originalRequest.url

                val urlWithApi = originalHttpUrl.newBuilder()
                    .addQueryParameter("api_key", API_KEY)
                    .build()

                // Request customization: add request headers
                val requestBuilder = originalRequest.newBuilder()
                    .url(urlWithApi)

                val request = requestBuilder.build()
                chain.proceed(request)
            }

            return httpClient.build()
        }

        fun getInstance(): Retrofit {
            if (retrofit == null) {
                retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(getOkHttpClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            }
            return retrofit!!
        }
    }
}