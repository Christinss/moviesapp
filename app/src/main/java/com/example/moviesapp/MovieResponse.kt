package com.example.moviesapp

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize


////This class is for getting multiple movies (Movie list)
@Parcelize
data class MovieResponse (
    @SerializedName("results")
    val movies : List<Movie>
) : Parcelable
