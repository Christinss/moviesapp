package com.example.moviesapp

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

interface RecyclerViewPagingHelper {

    var threshold: Int

    fun attach(recyclerView: RecyclerView, onScrolledToEndListener: () -> Unit)

    fun detach()

}
class MovieRecyclerViewPagingHelper : RecyclerViewPagingHelper, RecyclerView.OnScrollListener() {



        //number of items per page
        override var threshold: Int = 5

        private var recyclerView: RecyclerView? = null

        private var onScrolledToEndListener: (() -> Unit)? = null

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)

            //this will not work for vertical recyclerView
            if (dy <= 0) {
                return
            }

            val layoutManager = recyclerView.layoutManager
            //we support only LinearLayoutManager.
            if (layoutManager is GridLayoutManager) {
                val lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition()
                val itemCount = layoutManager.itemCount

                if ((lastVisibleItemPosition + threshold) >= itemCount - 1) {
                    onScrolledToEndListener?.invoke()
                }
            }
        }

        override fun attach(recyclerView: RecyclerView, onScrolledToEndListener: () -> Unit) {
            recyclerView.addOnScrollListener(this)
            this.recyclerView = recyclerView
            this.onScrolledToEndListener = onScrolledToEndListener
        }

        override fun detach() {
            recyclerView?.removeOnScrollListener(this)
            onScrolledToEndListener = null
        }
}